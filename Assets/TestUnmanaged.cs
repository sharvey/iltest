﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using UnityEngine;
using System.Linq;
using System.IO;

public static class TypeHelper
{
	public static Type CreateMulticast<T>(ModuleBuilder builder)
	{
		var newType = builder.DefineType(
			$"{typeof(T).Name}_Multicast", 
			TypeAttributes.Class | TypeAttributes.Public,
			null, 
			new[] { typeof(T) }
		);

		ILGenerator il;

		var field_a = newType.DefineField("a", typeof(T), FieldAttributes.Private);
		var field_b = newType.DefineField("b", typeof(T), FieldAttributes.Private);

		var constructorBuilder = newType.DefineConstructor(MethodAttributes.Public, CallingConventions.HasThis, new[] { typeof(T), typeof(T) });
		il = constructorBuilder.GetILGenerator();
		il.Emit(OpCodes.Ldarg_0);
		il.Emit(OpCodes.Ldarg_1);
		il.Emit(OpCodes.Stfld, field_a);
		il.Emit(OpCodes.Ldarg_0);
		il.Emit(OpCodes.Ldarg_2);
		il.Emit(OpCodes.Stfld, field_b);
		il.Emit(OpCodes.Ret);

		var info = typeof(T).GetTypeInfo();
		var methods = info.GetMethods();
		foreach (var m in methods)
		{
			var paramTypes = m.GetParameters().Select(p => p.GetType()).ToArray();
			var methodBuilder = newType.DefineMethod(m.Name, 
				MethodAttributes.Public | MethodAttributes.HideBySig |
				MethodAttributes.NewSlot | MethodAttributes.Virtual |
				MethodAttributes.Final,
				null, paramTypes);
			il = methodBuilder.GetILGenerator();

			var nargs = m.GetParameters().Length;

			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, field_a);
			for (int i=0; i<nargs; ++i)
			{
				il.Emit(OpCodes.Ldarg, i+1);
			}
			
			il.Emit(OpCodes.Call, typeof(T).GetMethod(m.Name));

			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, field_b);
			for (int i = 0; i < nargs; ++i)
			{
				il.Emit(OpCodes.Ldarg, i + 1);
			}

			il.Emit(OpCodes.Call, typeof(T).GetMethod(m.Name));
			il.Emit(OpCodes.Ret);

			newType.DefineMethodOverride(methodBuilder, m);
		}
		
		var result = newType.CreateType();
		return result;
	}
}

public interface ILogger
{
	void Log(string msg, string foo);
}

class TestLogger : ILogger
{
	private string _prefix;

	public TestLogger(string prefix)
	{
		_prefix = prefix;
	}

	public void Log(string msg, string foo)
	{
		Debug.Log($"[{_prefix}] {msg} {foo}");
	}

	public void Hello()
	{

	}
}

public class TestUnmanaged : MonoBehaviour
{
	void Start()
    {
		//Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);

		string asmFilename = "GeneratedTypes.dll";

		ILogger foo = new TestLogger("Foo");
		ILogger bar = new TestLogger("Bar");

		AssemblyBuilder builder;

		builder = Thread.GetDomain().DefineDynamicAssembly(
			new AssemblyName(Guid.NewGuid().ToString()), AssemblyBuilderAccess.RunAndSave
		);

		//builder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(Guid.NewGuid().ToString()), AssemblyBuilderAccess.Save);

		var moduleBuilder = builder.DefineDynamicModule(asmFilename, asmFilename);

		var multi = TypeHelper.CreateMulticast<ILogger>(moduleBuilder);

		builder.Save($"{asmFilename}");

		if (!Directory.Exists("Assets/Plugins/"))
		{
			Directory.CreateDirectory("Assets/Plugins/");
		}

		File.Copy(asmFilename, $"Assets/Plugins/{asmFilename}", true);
		File.Delete(asmFilename);

		//File.Move(asmFilename, $"Assets/Plugins/{asmFilename}");
		//builder.Save($"Assets/Plugins/{asmFilename}");

		var test = Activator.CreateInstance(multi, foo, bar);
		
		((ILogger)test).Log("My message", "bar");
    }

    void Update()
    {
        
    }
}
